# Principles and practices for managing and structuring data

Slides (44) for a 1-2h seminar on good management practices of research data and documents.

Language: English.

[Version française](https://umr-astre.pages.mia.inra.fr/training/gestion-donnees/)

Topics:

- Data storage

- Naming files (and variables)

- Organising files

- Structuring data

- Tidy data

- The description of a data set

[![](img/title-page.png)](https://umr-astre.pages.mia.inra.fr/training/data-management/)

<a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr"><img alt="Licence Creative Commons" style="border-width:0" src="https://i.creativecommons.org/l/by-sa/4.0/88x31.png" /></a><br />Cette œuvre est mise à disposition selon les termes de la <a rel="license" href="https://creativecommons.org/licenses/by-sa/4.0/deed.fr">Licence Creative Commons Attribution -  Partage dans les Mêmes Conditions 4.0 International</a>.
